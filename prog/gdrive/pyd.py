from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive


class GDrive(object):

    """
        Estabelece as validacao das credenciais com a API do GDrive
    """
    def __init__(self):
        self._gauth = GoogleAuth()
        self._gauth.LocalWebserverAuth()
        self._drive = GoogleDrive(self._gauth)

    """
        Lista os arquivos que contenham no nome 'acervo' e que sejam json,
        desde que nao estejam na lixeira
    """
    def list_files(self):
        self.file_list = self._drive.ListFile(
             {'q':
                 "(title contains 'acervo') and trashed=false \
                   and (mimeType='application/json')"}
             ).GetList()
        return (self.file_list)

    """
        Faz o download do arquivo com base no id
    """
    def download(self, name, id):
        f = self._drive.CreateFile({'id': id})
        f.GetContentFile('./'+name)
