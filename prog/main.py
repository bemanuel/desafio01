import os
import pandas as pd
from tinydb import TinyDB
from gdrive.pyd import GDrive


def process():
    dr = GDrive()
    for f in dr.list_files():
        n = f['title']
        dr.download(n, f['id'])
        db = TinyDB('./'+n)
        df = pd.DataFrame(db.all())
        n2 = os.path.splitext(n)[0]
        df.to_csv('./'+n2+'.csv')
        db.close()
        os.remove(n)


if __name__ == '__main__':
    process()
