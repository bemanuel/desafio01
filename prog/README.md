# Sistema para leitura de base

## Requisitos

 - Google Drive API
 - pipenv
 - pip
 - python 3.8

## Preparando ambiente

- Acesse o Google Drive API e o ative  (Opção Desktop APP)
- Guarde as credenciais do Google Drive API geradas (contidos no arquivo credentials.json)
- Arquivo credentials.json deve ser colocado na mesma pasta do aplicativo com o nome **client_secrets.json**

```bash
pip install pipenv
pipenv sync
```

## Rodando o sistema

```bash
pipenv shell
python main.py
```
